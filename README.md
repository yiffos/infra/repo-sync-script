# Repo Sync Script
Script to sync a local yiffOS repository with a remote one. Intended for mirrors.

### Usage
1. Copy `sync.sh.template` to `sync.sh` and modify the configuration embedded into the script.
2. Chmod (`chmod +x sync.sh`) it.
3. Throw it into a cron job to run every so often (e.g. 1 hour).